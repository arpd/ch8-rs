extern crate ch8_rs;

use structopt::StructOpt;
use std::path::PathBuf;
use std::fs::read;
use ch8_rs::icodec;

#[derive(StructOpt, Debug)]
#[structopt(name = "foo")]
struct Options {
    // The number of occurences of the `v/verbose` flag
    /// verbosity (-v[v[v...]]); additional occurrences increase the amount of
    /// debug output.
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,

    /// chip8 binary to disassemble; assembly is read from stdin if omitted
    #[structopt(parse(from_os_str))]
    input: Option<PathBuf>
}

fn main() -> std::io::Result<()> {
    let opts = Options::from_args();

    if let Some(path) = opts.input {
        let data = read(&path).unwrap();
        for i in (0..data.len()).step_by(2) {
            print!("0x{:04x}:\t", i);
            let raw: u16 = ((data[i] as u16) << 8) | (data[i+1] as u16);
            let op = icodec::Operation::new(raw);
            if let icodec::OperationType::Unsupported(_) = op.op_type {
                panic!("Unsupported op 0x{:04x} {:?}", raw, op);
            }
            println!("{}", op.mnemonic());
        }
    } else {
        let o = icodec::Operation::new(0u16);
        println!("{:?}", o);
        println!("{}", o.mnemonic());
        println!("{}", icodec::Operation::new(0x1cf3u16).mnemonic());
        println!("{}", icodec::Operation::new(0x0ff3u16).mnemonic());
        println!("{}", icodec::Operation::new(0xbeefu16).mnemonic());
    }
    Ok(())
}
