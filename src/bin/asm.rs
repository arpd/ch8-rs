extern crate ch8_rs;

use structopt::StructOpt;
use std::path::PathBuf;
use std::fs::read_to_string;
use ch8_rs::icodec::asm::*;
use nom::IResult;
use nom::types::CompleteStr;

#[derive(StructOpt, Debug)]
#[structopt(name = "foo")]
struct Options {
    // The number of occurences of the `v/verbose` flag
    /// verbosity (-v[v[v...]]); additional occurrences increase the amount of
    /// debug output.
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,

    /// chip8 source file to assemble; source is read from stdin if omitted.
    #[structopt(parse(from_os_str))]
    input: Option<PathBuf>,

    /// output file; output is written to stdout if omitted
    #[structopt(parse(from_os_str))]
    output: Option<PathBuf>,
}

fn main() -> std::io::Result<()> {
    let opts = Options::from_args();

    if let Some(path) = opts.input {
        //let data = read_to_string(path).unwrap();
        //let (remaining, result) = mnemonic(data[..].as_bytes()).unwrap();
        //println!("{:?}", result);
        //loop {
        //    let (remaining, result) = mnemonic(remaining).unwrap();
        //    println!("{:?}", result);
        //}
    } else {
        let src = CompleteStr("v2\n");
        let (r,_) = reg_gp(src).unwrap();
        println!("{:?}", r);
    }
    Ok(())
}
