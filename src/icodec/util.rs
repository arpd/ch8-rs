/// This macro returns a half closed interval for ranges, i.e. the bit values
/// returned by `xbits(0xbeef:0..4)` and `xbits(0xbeef:4..8)` do not overlap.
/// Example usage:
///   xbits!(0b11110000:4)    -> 0b1
///   xbits!(0b11110000:2..6) -> 0b1100
macro_rules! xbits {
    ($val:tt:$idx:tt) => {
        (
            ($val & (0b1 << ($idx))) >> ($idx)
        )
    };
    ($val:tt:$start:tt..$end:tt) => {
        (
            ($val >> ($start) & (0b1 << ($end - $start)) - 1)
        )
    };
}

macro_rules! disas_fmt {
    ($name: tt) => {
        format!("{:>6}", stringify!($name))
    };
    ($name: tt, $fmt: tt, $($arg:tt)*) => {
        format!(concat!("{:>6}\t", $fmt), stringify!($name), $($arg)*)
    };
}
