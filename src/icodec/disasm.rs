use super::OperationType;

/// Extract a single bit, or a range of bits from an integer.
pub fn decode(raw: u16) -> OperationType {
    match raw {
        0x00e0          => { OperationType::ClearScreen },
        0x00ee          => { OperationType::Return },
        0x0000...0x0fff => { OperationType::SystemCall(xbits!(raw:0..12) as u16) },
        0x1000...0x1fff => { OperationType::Jump(xbits!(raw:0..12) as u16) },
        0x2000...0x2fff => { OperationType::Call(xbits!(raw:0..12) as u16) },
        0x3000...0x3fff => {
            OperationType::SkipEqualLiteral(xbits!(raw:8..12) as usize,
                                            xbits!(raw:0..8) as u8)
        },
        0x4000...0x4fff => {
            OperationType::SkipNotEqualLiteral(xbits!(raw:8..12) as usize,
                                               xbits!(raw:0..8) as u8)
        },
        0x5000...0x5ff0 => {
            OperationType::SkipEqualRegister(xbits!(raw:8..12) as usize,
                                             xbits!(raw:4..8) as usize)
        },
        0x6000...0x6fff => {
            OperationType::LoadLiteral(xbits!(raw:8..12) as usize,
                                       xbits!(raw:0..8) as u8)
        },
        0x7000...0x7fff => {
            OperationType::AddLiteral(xbits!(raw:8..12) as usize,
                                      xbits!(raw:0..8) as u8)
        },
        0x8000...0x8fff => match raw & 0xf {
            0x0 => {
                OperationType::LoadRegister(xbits!(raw:8..12) as usize,
                                            xbits!(raw:4..8) as usize)
            },
            0x1 => {
                OperationType::OrRegister(xbits!(raw:8..12) as usize,
                                          xbits!(raw:4..8) as usize)
            },
            0x2 => {
                OperationType::AndRegister(xbits!(raw:8..12) as usize,
                                           xbits!(raw:4..8) as usize)
            },
            0x3 => {
                OperationType::XorRegister(xbits!(raw:8..12) as usize,
                                           xbits!(raw:4..8) as usize)
            },
            0x4 => {
                OperationType::AddRegister(xbits!(raw:8..12) as usize,
                                           xbits!(raw:4..8) as usize)
            },
            0x5 => {
                OperationType::SubRegister(xbits!(raw:8..12) as usize,
                                           xbits!(raw:4..8) as usize)
            },
            0x6 => {
                OperationType::ShiftRightRegister(xbits!(raw:8..12) as usize,
                                                  xbits!(raw:4..8) as usize)
            },
            0x7 => {
                OperationType::SubNoBorrowRegister(xbits!(raw:8..12) as usize,
                                                   xbits!(raw:4..8) as usize)
            },
            0xe => {
                OperationType::ShiftLeftRegister(xbits!(raw:8..12) as usize,
                                                 xbits!(raw:4..8) as usize)
            },
            _ => {
                OperationType::Unsupported(raw)
            },
        },
        0x9000...0x9fff => match raw & 0xf {
            0x0 => {
                OperationType::SkipNotEqualRegister(xbits!(raw:8..12) as usize,
                                                    xbits!(raw:4..8) as usize)
            },
            _ => {
                OperationType::Unsupported(raw)
            }
        },
        0xa000...0xafff => {
            OperationType::LoadILiteral(xbits!(raw:0..12) as u16)
        },
        0xb000...0xbfff => {
            OperationType::JumpOffset(xbits!(raw:0..12) as u16)
        },
        0xc000...0xcfff => {
            OperationType::RandomAndLiteral(xbits!(raw:8..12) as usize,
                                            xbits!(raw:0..8) as u8)
        },
        0xd000...0xdfff => {
            OperationType::DrawSprite(xbits!(raw:8..12) as usize,
                                      xbits!(raw:4..8) as usize,
                                      xbits!(raw:0..4) as u8)
        },
        0xe09e...0xefa1 => match raw & 0xff {
            0x9e => {
                OperationType::SkipPressed(xbits!(raw:8..12) as usize)
            },
            0xa1 => {
                OperationType::SkipNotPressed(xbits!(raw:8..12) as usize)
            },
            _ => {
                OperationType::Unsupported(raw)
            }
        },
        0xf007...0xff65 => match raw & 0xff {
            0x07 => {
                OperationType::LoadDelayTimer(xbits!(raw:8..12) as usize)
            },
            0x0a => {
                OperationType::WaitKey(xbits!(raw:8..12) as usize)
            },
            0x15 => {
                OperationType::StoreDelayTimer(xbits!(raw:8..12) as usize)
            },
            0x18 => {
                OperationType::StoreSoundTimer(xbits!(raw:8..12) as usize)
            },
            0x1e => {
                OperationType::AddIRegister(xbits!(raw:8..12) as usize)
            },
            0x29 => {
                OperationType::LoadISprite(xbits!(raw:8..12) as usize)
            },
            0x33 => {
                OperationType::StoreRegisterBCDAtI(xbits!(raw:8..12) as usize)
            },
            0x55 => {
                OperationType::StoreRegistersAtI(xbits!(raw:8..12) as usize)
            },
            0x65 => {
                OperationType::LoadRegistersFromI(xbits!(raw:8..12) as usize)
            },
            _ => {
                OperationType::Unsupported(raw)
            }
        },

        _ => {
            OperationType::Unsupported(raw)
        }
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]
    use super::super::Operation;

    #[test]
    fn SystemCall() {
        for i in 0x000..0xfff + 1 {
            if i == 0x00e0 || i == 0x00ee {
                continue;
            }
            let raw_op = 0x0000 | i;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(), disas_fmt!(sys, "#0x{:04x}", i));
        }
    }

    #[test]
    fn ClearScreen() {
        let op = Operation::new(0x00e0);
        assert_eq!(op.mnemonic(), disas_fmt!(cls));
    }

    #[test]
    fn Return() {
        let op = Operation::new(0x00ee);
        assert_eq!(op.mnemonic(), disas_fmt!(ret));
    }

    #[test]
    fn Jump() {
        for i in 0x000..0xfff + 1 {
            let raw_op = 0x1000 | i;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(), disas_fmt!(jmp, "#0x{:04x}", i));
        }
    }

    #[test]
    fn SkipEqualLiteral() {
        for reg in 0x0..0xf + 1 {
            for lit in 0x00 .. 0xff + 1 {
                let mut raw_op: u16 = 0x3000;
                raw_op |= reg << 8;
                raw_op |= lit;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(seq, "v{},\t#0x{:02x}",
                                                  reg, lit));
            }
        }
    }

    #[test]
    fn SkipNotEqualLiteral() {
        for reg in 0x0..0xf + 1 {
            for lit in 0x00 .. 0xff + 1 {
                let mut raw_op: u16 = 0x4000;
                raw_op |= reg << 8;
                raw_op |= lit;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(sne, "v{},\t#0x{:02x}",
                                                  reg, lit));
            }
        }
    }

    #[test]
    fn SkipEqualRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x5000;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(seq, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn LoadLiteral() {
        for reg in 0x0..0xf + 1 {
            for lit in 0x00 .. 0xff + 1 {
                let mut raw_op: u16 = 0x6000;
                raw_op |= reg << 8;
                raw_op |= lit;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(ld, "v{},\t#0x{:02x}",
                                                  reg, lit));
            }
        }
    }

    #[test]
    fn AddLiteral() {
        for reg in 0x0..0xf + 1 {
            for lit in 0x00 .. 0xff + 1 {
                let mut raw_op: u16 = 0x7000;
                raw_op |= reg << 8;
                raw_op |= lit;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(add, "v{},\t#0x{:02x}",
                                                  reg, lit));
            }
        }
    }

    #[test]
    fn LoadRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x8000;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(ld, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn OrRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x8001;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(or, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn AndRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x8002;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(and, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn XorRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x8003;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(xor, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn AddRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x8004;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(add, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn SubRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x8005;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(sub, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn ShiftRightRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x8006;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(shr, "v{}[,	v{}]",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn SubNoBorrowRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x8007;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(subn, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn ShiftLeftRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x800e;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(shl, "v{}[,	v{}]",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn SkipNotEqualRegister() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                let mut raw_op: u16 = 0x9000;
                raw_op |= reg_x << 8;
                raw_op |= reg_y << 4;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(sne, "v{},\tv{}",
                                                  reg_x, reg_y));
            }
        }
    }

    #[test]
    fn LoadILiteral() {
        for lit in 0x000..0xfff + 1 {
            let mut raw_op: u16 = 0xa000;
            raw_op |= lit;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(), disas_fmt!(ld, "I,\t#0x{:04x}",
                                              lit));
        }
    }

    #[test]
    fn JumpOffset() {
        for lit in 0x000..0xfff + 1 {
            let mut raw_op: u16 = 0xb000;
            raw_op |= lit;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(), disas_fmt!(jmp, "v0, #0x{:04x}",
                                              lit));
        }
    }

    #[test]
    fn RandomAndLiteral() {
        for reg in 0x0..0xf + 1 {
            for lit in 0x00 .. 0xff + 1 {
                let mut raw_op: u16 = 0xc000;
                raw_op |= reg << 8;
                raw_op |= lit;
                let op = Operation::new(raw_op);
                assert_eq!(op.mnemonic(), disas_fmt!(rnd, "v{},\t#0x{:02x}",
                                                  reg, lit));
            }
        }
    }

    #[test]
    fn DrawSprite() {
        for reg_x in 0x0..0xf + 1 {
            for reg_y in 0x0..0xf + 1 {
                for lit in 0x0..0xf + 1 {

                    let mut raw_op: u16 = 0xd000;
                    raw_op |= reg_x << 8;
                    raw_op |= reg_y << 4;
                    raw_op |= lit;
                    let op = Operation::new(raw_op);
                    assert_eq!(op.mnemonic(),
                               disas_fmt!(draw, "v{},\tv{},\t#0x{:01x}",
                                       reg_x, reg_y, lit));
                }
            }
        }
    }

    #[test]
    fn SkipPressed() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xe09e;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(sp, "v{}", reg));
        }
    }

    #[test]
    fn SkipNotPressed() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xe0a1;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(snp, "v{}", reg));
        }
    }

    #[test]
    fn LoadDelayTimer() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf007;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(ld, "v{},\tDT", reg));
        }
    }

    #[test]
    fn WaitKey() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf00a;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(wk, "v{}", reg));
        }
    }

    #[test]
    fn SetDelayTimer() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf015;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(ld, "DT, v{}", reg));
        }
    }

    #[test]
    fn SetSoundTimer() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf018;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(ld, "ST, v{}", reg));
        }
    }

    #[test]
    fn AddIRegister() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf01e;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(add, "I,\tv{}", reg));
        }
    }

    #[test]
    fn LoadISprite() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf029;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(ld, "I,\t[@v{}]", reg));
        }
    }

    #[test]
    fn StoreRegisterBCDAtI() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf033;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(ldb, "[I],\tv{}", reg));
        }
    }

    #[test]
    fn StoreRegistersAtI() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf055;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(ld, "[I],\tv0..v{}", reg));
        }
    }

    #[test]
    fn LoadRegistersFromI() {
        for reg in 0x0..0xf + 1 {
            let mut raw_op: u16 = 0xf065;
            raw_op |= reg << 8;
            let op = Operation::new(raw_op);
            assert_eq!(op.mnemonic(),
                       disas_fmt!(ld, "v0..v{},\t[I]", reg));
        }
    }
}
