#[macro_use] mod util;
mod disasm;
pub mod asm;

#[derive(Debug)]
pub enum OperationType {
    SystemCall(u16),
    ClearScreen,
    Return,
    Jump(u16),
    Call(u16),
    SkipEqualLiteral(usize, u8),
    SkipNotEqualLiteral(usize, u8),
    SkipEqualRegister(usize, usize),
    LoadLiteral(usize, u8),
    LoadRegister(usize, usize),
    AddLiteral(usize, u8),
    OrRegister(usize, usize),
    AndRegister(usize, usize),
    XorRegister(usize, usize),
    AddRegister(usize, usize),
    SubRegister(usize, usize),
    ShiftRightRegister(usize, usize),
    SubNoBorrowRegister(usize, usize),
    ShiftLeftRegister(usize, usize),
    SkipNotEqualRegister(usize, usize),
    LoadILiteral(u16),
    JumpOffset(u16),
    RandomAndLiteral(usize, u8),
    DrawSprite(usize, usize, u8),
    SkipPressed(usize),
    SkipNotPressed(usize),
    LoadDelayTimer(usize),
    WaitKey(usize),
    StoreDelayTimer(usize),
    StoreSoundTimer(usize),
    AddIRegister(usize),
    LoadISprite(usize),
    StoreRegisterBCDAtI(usize),
    StoreRegistersAtI(usize),
    LoadRegistersFromI(usize),

    Unsupported(u16),
}

#[derive(Debug)]
pub struct Operation {
    pub raw: u16,
    pub op_type: OperationType,
}


impl Operation {
    pub fn new(raw: u16) -> Operation {
        Operation {
            raw: raw,
            op_type: disasm::decode(raw)
        }
    }

    pub fn mnemonic(&self) -> String {
        match self.op_type {
            OperationType::SystemCall(addr) =>
                disas_fmt!(sys, "#0x{:04x}", addr),
            OperationType::ClearScreen =>
                disas_fmt!(cls),
            OperationType::Return =>
                disas_fmt!(ret),
            OperationType::Jump(addr) =>
                disas_fmt!(jmp, "#0x{:04x}", addr),
            OperationType::Call(addr) =>
                disas_fmt!(call, "#0x{:04x}", addr),
            OperationType::SkipEqualLiteral(reg, lit) =>
                disas_fmt!(seq, "v{},\t#0x{:02x}", reg, lit),
            OperationType::SkipNotEqualLiteral(reg, lit) =>
                disas_fmt!(sne, "v{},\t#0x{:02x}", reg, lit),
            OperationType::SkipEqualRegister(reg_x, reg_y) =>
                disas_fmt!(seq, "v{},\tv{}", reg_x, reg_y),
            OperationType::LoadLiteral(reg, lit) =>
                disas_fmt!(ld, "v{},\t#0x{:02x}", reg, lit),
            OperationType::LoadRegister(reg_x, reg_y) =>
                disas_fmt!(ld, "v{},\tv{}", reg_x, reg_y),
            OperationType::AddLiteral(reg, lit) =>
                disas_fmt!(add, "v{},\t#0x{:02x}", reg, lit),
            OperationType::OrRegister(reg_x, reg_y) =>
                disas_fmt!(or, "v{},\tv{}", reg_x, reg_y),
            OperationType::AndRegister(reg_x, reg_y) =>
                disas_fmt!(and, "v{},\tv{}", reg_x, reg_y),
            OperationType::XorRegister(reg_x, reg_y) =>
                disas_fmt!(xor, "v{},\tv{}", reg_x, reg_y),
            OperationType::AddRegister(reg_x, reg_y) =>
                disas_fmt!(add, "v{},\tv{}", reg_x, reg_y),
            OperationType::SubRegister(reg_x, reg_y) =>
                disas_fmt!(sub, "v{},\tv{}", reg_x, reg_y),
            OperationType::ShiftRightRegister(reg_x, reg_y) =>
                disas_fmt!(shr, "v{}[,	v{}]", reg_x, reg_y),
            OperationType::SubNoBorrowRegister(reg_x, reg_y) =>
                disas_fmt!(subn, "v{},\tv{}", reg_x, reg_y),
            OperationType::ShiftLeftRegister(reg_x, reg_y) =>
                disas_fmt!(shl, "v{}[,	v{}]", reg_x, reg_y),
            OperationType::SkipNotEqualRegister(reg_x, reg_y) =>
                disas_fmt!(sne, "v{},\tv{}", reg_x, reg_y),
            OperationType::LoadILiteral(lit) =>
                disas_fmt!(ld, "I,\t#0x{:04x}", lit),
            OperationType::JumpOffset(lit) =>
                disas_fmt!(jmp, "v0, #0x{:04x}", lit),
            OperationType::RandomAndLiteral(reg, lit) =>
                disas_fmt!(rnd, "v{},\t#0x{:02x}", reg, lit),
            OperationType::DrawSprite(reg_x, reg_y, lit) =>
                disas_fmt!(draw, "v{},\tv{},\t#0x{:01x}", reg_x, reg_y, lit),
            OperationType::SkipPressed(reg) =>
                disas_fmt!(sp, "v{}", reg),
            OperationType::SkipNotPressed(reg) =>
                disas_fmt!(snp, "v{}", reg),
            OperationType::LoadDelayTimer(reg) =>
                disas_fmt!(ld, "v{},\tDT", reg),
            OperationType::WaitKey(reg) =>
                disas_fmt!(wk, "v{}", reg),
            OperationType::StoreDelayTimer(reg) =>
                disas_fmt!(ld, "DT, v{}", reg),
            OperationType::StoreSoundTimer(reg) =>
                disas_fmt!(ld, "ST, v{}", reg),
            OperationType::AddIRegister(reg) =>
                disas_fmt!(add, "I,\tv{}", reg),
            OperationType::LoadISprite(reg) =>
                disas_fmt!(ld, "I,\t[@v{}]", reg),
            OperationType::StoreRegisterBCDAtI(reg) =>
                disas_fmt!(ldb, "[I],\tv{}", reg),
            OperationType::StoreRegistersAtI(reg) =>
                disas_fmt!(ld, "[I],\tv0..v{}", reg),
            OperationType::LoadRegistersFromI(reg) =>
                disas_fmt!(ld, "v0..v{},\t[I]", reg),

            OperationType::Unsupported(raw) =>
                format!("UNSUPPORTED OP: 0x{:04x}", raw),
        }
    }
}
