//use super::{OperationType};
//use std::str;
use nom::{digit, alpha1, multispace};
use nom::types::CompleteStr;

#[derive(Debug, PartialEq)]
pub enum Mnemonic {
    Add,
    And,
    Call,
    Cls,
    Draw,
    Jmp,
    Ld,
    Ldb,
    Or,
    Ret,
    Rnd,
    Seq,
    Shl,
    Shr,
    Sne,
    Snp,
    Sp,
    Sub,
    Subn,
    Sys,
    Wk,
    Xor,
    Undefined,
}

impl<'a> From<CompleteStr<'a>> for Mnemonic {
    fn from(v: CompleteStr<'a>) -> Self {
        match v {
            CompleteStr("add")  => Mnemonic::Add,
            CompleteStr("and")  => Mnemonic::And,
            CompleteStr("call") => Mnemonic::Call,
            CompleteStr("cls")  => Mnemonic::Cls,
            CompleteStr("draw") => Mnemonic::Draw,
            CompleteStr("jmp")  => Mnemonic::Jmp,
            CompleteStr("ld")   => Mnemonic::Ld,
            CompleteStr("ldb")  => Mnemonic::Ldb,
            CompleteStr("or")   => Mnemonic::Or,
            CompleteStr("ret")  => Mnemonic::Ret,
            CompleteStr("rnd")  => Mnemonic::Rnd,
            CompleteStr("seq")  => Mnemonic::Seq,
            CompleteStr("shl")  => Mnemonic::Shl,
            CompleteStr("shr")  => Mnemonic::Shr,
            CompleteStr("sne")  => Mnemonic::Sne,
            CompleteStr("snp")  => Mnemonic::Snp,
            CompleteStr("sp")   => Mnemonic::Sp,
            CompleteStr("sub")  => Mnemonic::Sub,
            CompleteStr("subn") => Mnemonic::Subn,
            CompleteStr("sys")  => Mnemonic::Sys,
            CompleteStr("wk")   => Mnemonic::Wk,
            CompleteStr("xor")  => Mnemonic::Xor,
            _                   => Mnemonic::Undefined,
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Operand {
    RegisterGP(u8),
    RegisterI,
    RegisterDT,
    RegisterST,
    Literal(u16),
    Indirect(u16),
}

// FIXME: Change operands from Vec<Operand> to tuple of Option(Operand)
#[derive(Debug, PartialEq)]
pub struct Instruction {
    pub mnemonic: Mnemonic,
    pub operands: Vec<Operand>,
    //pub src_line: u8,
}

// FIXME: Pull parsing out to a module of its own
named!(pub literal<CompleteStr, Operand>,
       ws!(
           do_parse!(
               tag!("#") >>
               v: digit  >>
               (
                   Operand::Literal(v.parse::<u16>().unwrap())
               )
           )
       )
);

named!(pub indirect_val<CompleteStr, CompleteStr>,
       ws!(
           delimited!(
               tag!("["),
               digit,
               tag!("]")
           )
       )
);

named!(pub indirect<CompleteStr, Operand>,
       ws!(
           do_parse!(
               v: indirect_val >>
               (
                   Operand::Indirect(v.parse::<u16>().unwrap())
               )
           )
       )
);

named!(pub reg_special<CompleteStr, Operand>,
       ws!(
           alt_complete!(
               tag_no_case!("dt") => { |_| Operand::RegisterDT } |
               tag_no_case!("st") => { |_| Operand::RegisterST } |
               tag_no_case!("i")  => { |_| Operand::RegisterI  }
           )
       )
);

named!(pub reg_gp<CompleteStr, Operand>,
       ws!(
           do_parse!(
               tag_no_case!("v") >>
               v: digit >>
               (
                   Operand::RegisterGP(v.parse::<u8>().unwrap())
               )
           )
       )
);

named!(pub operand<CompleteStr, Operand>,
       alt!(
           reg_gp |
           reg_special |
           indirect |
           literal
       )
);

named!(operands <CompleteStr, Vec<Operand> >,
       many0!(operand)
);

named!(pub mnemonic<CompleteStr, Mnemonic>,
       do_parse!(
           mnemonic_str: alpha1 >>
           (
               Mnemonic::from(mnemonic_str)
           )
       )
);

named!(pub instruction<CompleteStr, Instruction>,
       do_parse!(
           m: mnemonic >>
           opt!(multispace) >>
           o: operands >>
           (
               Instruction {
                   mnemonic: m,
                   operands: o
               }
           )
       )
);

mod tests {
    use super::*;
    macro_rules! assert_rval_eq {
        ($p:expr,$s:expr,$e:expr) => (
            assert_eq!($p($s).unwrap().1, $e)
        )
    }

    macro_rules! assert_rval_ne {
        ($p:expr,$s:expr,$e:expr) => (
            assert_ne!($p($s).unwrap().1, $e)
        )
    }

    macro_rules! assert_ok_eq {
        ($p:expr,$s:expr,$v:expr) => (
            assert_eq!($p($s).is_ok(), $v)
        )
    }

    // TODO: I'm not sure this makes a great deal of sense when using CompleteStr
    macro_rules! assert_exhausted_eq {
        ($p:expr,$s:expr,$v:expr) => (
            assert_eq!($p($s).unwrap().0 == CompleteStr(""), $v)
        )
    }

    #[test]
    fn parse_literal() {
        assert_rval_eq!(literal, CompleteStr("#123"), Operand::Literal(123));
        assert_ok_eq!(literal, CompleteStr("#123"), true);
        assert_ok_eq!(literal, CompleteStr("123"), false);
        assert_ok_eq!(literal, CompleteStr("123#"), false);
        assert_ok_eq!(literal, CompleteStr("###123"), false);
    }

    #[test]
    fn parse_indirect() {
        assert_rval_eq!(indirect, CompleteStr("[123]"), Operand::Indirect(123));
        assert_ok_eq!(indirect, CompleteStr("[123]"), true);
        assert_ok_eq!(indirect, CompleteStr("[ 123]"), true);
        assert_ok_eq!(indirect, CompleteStr("[123 ]"), true);
        assert_ok_eq!(indirect, CompleteStr("[ 123 ]"), true);
        assert_ok_eq!(indirect, CompleteStr("\t[ 123 ]"), true);
        assert_ok_eq!(indirect, CompleteStr("[12 3]"), false);
        assert_ok_eq!(indirect, CompleteStr("[123"), false);
        assert_ok_eq!(indirect, CompleteStr("[!123]"), false);
    }

    #[test]
    fn parse_reg_gp() {
        assert_rval_eq!(reg_gp, CompleteStr("v12"), Operand::RegisterGP(12));
        assert_ok_eq!(reg_gp, CompleteStr(" v12"), true);
        // NOTE: May want to replace usage of `ws!()` as we don't want to span
        // newlines; The following test should then fail; Alternatively we can
        // handle it earlier so we never see a preceding newline.
        assert_ok_eq!(reg_gp, CompleteStr("\t\nV1\t\t"), true);
    }

    #[test]
    fn parse_reg_special() {
        assert_rval_eq!(reg_special, CompleteStr("dt"), Operand::RegisterDT);
        assert_rval_eq!(reg_special, CompleteStr("DT"), Operand::RegisterDT);
        assert_ok_eq!(reg_special, CompleteStr("dt"), true);
        assert_rval_eq!(reg_special, CompleteStr("st"), Operand::RegisterST);
        assert_rval_eq!(reg_special, CompleteStr("ST"), Operand::RegisterST);
        assert_ok_eq!(reg_special, CompleteStr("st"), true);
        assert_rval_eq!(reg_special, CompleteStr("i"), Operand::RegisterI);
        assert_rval_eq!(reg_special, CompleteStr("I"), Operand::RegisterI);
        assert_ok_eq!(reg_special, CompleteStr("i"), true);
    }

    #[test]
    fn parse_mnemonic() {
        assert_rval_eq!(mnemonic, CompleteStr("call"), Mnemonic::Call);
        assert_ok_eq!(mnemonic, CompleteStr("call"), true);
        assert_rval_eq!(mnemonic, CompleteStr("undefined mnemonic"), Mnemonic::Undefined);
        assert_ok_eq!(mnemonic, CompleteStr("undefined mnemonic"), true);
        assert_exhausted_eq!(mnemonic, CompleteStr("undefined mnemonic"), false);
    }

    #[test]
    fn parse_operand() {
        assert_rval_eq!(operand, CompleteStr("v1"), Operand::RegisterGP(1));
        assert_rval_eq!(operand, CompleteStr("#123"), Operand::Literal(123));
        assert_rval_eq!(operand, CompleteStr("i"), Operand::RegisterI);
        assert_rval_eq!(operand, CompleteStr("dt"), Operand::RegisterDT);
        assert_rval_eq!(operand, CompleteStr("st"), Operand::RegisterST);
        assert_ok_eq!(operand, CompleteStr("@#$*!&^"), false);
    }

    #[test]
    fn parse_operands() {
        let expected = vec![Operand::RegisterGP(123), Operand::RegisterI, Operand::Indirect(123)];
        assert_rval_eq!(operands, CompleteStr("v123 I [123]"), expected);
        assert_ok_eq!(operands, CompleteStr("v123 I [123]"), true);
        assert_rval_ne!(operands, CompleteStr("v1 DT [5]"), expected);
    }

    #[test]
    fn parse_instruction() {
        let expected = Instruction {
            mnemonic: Mnemonic::Call,
            operands: vec![Operand::Literal(123)],
        };
        assert_rval_eq!(instruction, CompleteStr("call #123"), expected);

        let expected = Instruction {
            mnemonic: Mnemonic::Undefined,
            operands: vec![],
        };
        assert_rval_eq!(instruction, CompleteStr("invalid bytes [1]"), expected);
    }
}
